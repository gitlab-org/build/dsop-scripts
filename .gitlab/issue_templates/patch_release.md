## Release Information

* Version: `x.y.z`

## Checklist

Following [README.md] here are the list of tasks:

- [ ] Clone repos
- [ ] Checkout and sync `development` branch
- [ ] Apply changes from GitLab release
- [ ] Confirm changes are valid
- [ ] Commit changes to new branch
- [ ] Push the upgrade and create MR
- [ ] Inspect MRs and re-assign