## <code style="color : red">Use branch [pre-17.3](https://gitlab.com/gitlab-org/build/dsop-scripts/-/tree/pre-17.3?ref_type=heads) for releases before 17.3</code>

## Requirements

* `glab`
* `jq`
* `git`
* `docker-compose`
* Docker `buildx`
* `yq`

## Using the scripts

If you are running these scripts on OSX, insure that you are running an
updated version of BASH. The standard version of BASH on OSX is 3.2.57
which is too old for many of the new features that have been incorporated
into BASH and several of the scripts in this repo will not function
correctly.

Information on upgrading to a newer version of BASH on OSX can be found at
https://itnext.io/upgrading-bash-on-macos-7138bd1066ba

### Clone the repos

Clones the ubi hardened repos from dsop. Should only need to be run when the repos locations/names change or when new repos are added.

```
./clone.sh
```

### Checkout and sync the development branch

The `development` branch is what all our release MRs must target. It represents what has been merged into the system. This should
be run prior to any new work being created.

```
./sync-branch.sh
```

### Apply changes from the gitlab release

This checks out the `https://gitlab.com/gitlab-org/build/CNG` and applies the released changes from that repo to the local repo1 checkouts.

You need to specify the ubi tag you want to release:

```sh
./release-helper.sh <tag>
```

For example:

```sh
./release-helper.sh v17.3.0-ubi
```

Note: The `./release-helper.sh` script recursively copies every file/directory
under `sources` into each DSOP repo. In particular, if you make changes to
`sources/build-scripts/*`, you need to re-run `./release-helper.sh` to make the
changes take effect.

### Diff the changes that were made to the folders here

Diffs the changed files one-by-one. Visually inspect to ensure there aren't unexpected changes.

```
./show-diff.sh
```

### For non-patch releases, run a local test build

Pull and tag the UBI images (only need to do once):

```shell
./load-ubi-images.sh
```

Build all the images locally:

```
./build-script.sh <tag>
```

For example:

```
./build-script.sh v17.3.0-ubi
```

If everything succeeds you run the build cleanup to cleanup build logs prior to commit:

```
./clean-build.sh
```

### For non-patch releases, test the main images using docker-compose

You need both docker, and docker-compose installed for this to work.

1. Export the local tagged images. If you ran `./build-script.sh v17.3.0-ubi` for example. Your images would have been tagged as `17.3.0`
1. `export GITLAB_TAG=17.3.0`
1. Bring up the containers using docker compose `docker compose up -d`

Smoke test the images:

1. Run `docker compose ps -a`, until you see all containers in the desired state:
   * redis and postgres containers should have State `Up`
   * The migrations containers should be listed as `Exit 0`
   * All other containers should be `Up (healthy)`
   * This could take up to 3 minutes to reach this sate

   You likely will have to `down` and re-`up` to get the `webservice` container
   to start properly.
1. Reach the local gitlab instance by going to `localhost:3000` in your browser
1. Set your root password and login
1. Create a new project and initialize a Readme through the UI to confirm gitaly is working
1. Add your ssh key to your account, and clone the test project locally through ssh
1. Make a file change to the local git checkout of the test project, and push it back up over ssh.
1. Refresh the project in your browser to see the change. Confirming that shell works.
1. Run `docker compose down` to clean up the test.

### Commit changes to new branch

```
./commit-upgrade.sh <new-branch-name> "<commit message>"
```

For example:

```
./commit-upgrade.sh release-gitlab-17-3-0 "Update GitLab to the 17.3.0 Minor Feature Release"
```

Note that the commits must be signed by a gpg key that you have included for verification in the dsop GitLab account.

If you ran the build/test step earlier, it's also a good idea to take another look at `./show-diff.sh` to ensure no build assets were committed.

### Push the upgrade and create MRs

```
./push-upgrade.sh <new-branch-name> "<MR Title>" "<MR Description>"
```

For example:

```
./push-upgrade.sh release-gitlab-17-3-0 "Update GitLab to the 17.3.0 Minor Feature Release" "Adds the GitLab 17.3.0 Feature Release.<br><br>This is a regular monthly  feature release of GitLab.<br><br>More information can be found at https://about.gitlab.com/releases/2023/08/22/gitlab-17-3-released/<br><br>"
```

You will be asked for the username, and password (needs to be an access token).


This will create the MRs in the dsop repo

### Create application update issues on Repo1

#### Automated

**NOTE**: this step assumes that no MRs were filed after above created ones.

```shell
export GITLAB_TOKEN="token you've generated earlier for push" GITLAB_HOST="https://repo1.dso.mil"
./create_issues.sh "17.3.0"
```

#### Manual

You will need to manually check the MRs look correct over in the dsop repo, and assign them to the appropriate reviewer, as they will be assigned to you by default.

An issue must be created for each container in the corresponding repo1 project. This is currently a manual process.
1. Create a new issue
1. For format for issue titles is `container_name version` for example `gitlab pages 17.3.0`
1. Use the `Application Update` description template
1. Include a reference to the associated merge request in the issue description

### References
Ironbank Container Hardening Guide - https://docs-ironbank.dso.mil/hardening/overview/
