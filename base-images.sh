#!/usr/bin/env bash

if [ -z "${IMAGE_TAG:-}" ]; then
    echo "IMAGE_TAG not set." >&2
    exit 1
fi

UBI_TAG="${UBI_TAG:-9.5}"
UBI_BASE_IMAGE_REGISTRY="${UBI_BASE_IMAGE_REGISTRY:-local.localhost}"
UBI_IMAGE_BASE_REPO="${UBI_IMAGE_BASE_REPO:-redhat/ubi}"
UBI_IMAGE_MINIMAL_REPO="${UBI_IMAGE_MINIMAL_REPO:-${UBI_IMAGE_BASE_REPO}/ubi9-minimal}"
UBI_IMAGE_MINIMAL="${UBI_IMAGE_MINIMAL:-${UBI_BASE_IMAGE_REGISTRY}/${UBI_IMAGE_MINIMAL_REPO}:${UBI_TAG}}"
UBI_IMAGE_MICRO_REPO="${UBI_IMAGE_MICRO_REPO:-${UBI_IMAGE_BASE_REPO}/ubi9-micro}"
UBI_IMAGE_MICRO="${UBI_IMAGE_MICRO:-${UBI_BASE_IMAGE_REGISTRY}/${UBI_IMAGE_MICRO_REPO}:${UBI_TAG}}"
REDHAT_REGISTRY="${REDHAT_REGISTRY:-registry.access.redhat.com}"
# shellcheck disable=SC2034
REDHAT_UBI_IMAGE_MINIMAL="${REDHAT_REGISTRY}/ubi9/ubi-minimal:${UBI_TAG}"
# shellcheck disable=SC2034
REDHAT_UBI_IMAGE_MICRO="${REDHAT_REGISTRY}/ubi9/ubi-micro:${UBI_TAG}"

# shellcheck disable=SC2034
declare -A BASE_IMAGES=(
  [certificates]="gitlab/gitlab/gitlab-base:${IMAGE_TAG}"
  [gitaly]="gitlab/gitlab/gitlab-base:${IMAGE_TAG}"
  [gitlab-base]="${UBI_IMAGE_MICRO_REPO}:${UBI_TAG}"
  [gitlab-container-registry]="gitlab/gitlab/gitlab-base:${IMAGE_TAG}"
  [gitlab-exporter]="gitlab/gitlab/gitlab-ruby:${IMAGE_TAG}"
  [gitlab-kas]="${UBI_IMAGE_MICRO_REPO}:${UBI_TAG}"
  [gitlab-mailroom]="gitlab/gitlab/gitlab-ruby:${IMAGE_TAG}"
  [gitlab-pages]="gitlab/gitlab/gitlab-base:${IMAGE_TAG}"
  [gitlab-rails]="gitlab/gitlab/gitlab-ruby:${IMAGE_TAG}"
  [gitlab-ruby]="gitlab/gitlab/gitlab-base:${IMAGE_TAG}"
  [gitlab-shell]="gitlab/gitlab/gitlab-base:${IMAGE_TAG}"
  [gitlab-sidekiq]="gitlab/gitlab/gitlab-rails:${IMAGE_TAG}"
  [gitlab-toolbox]="gitlab/gitlab/gitlab-rails:${IMAGE_TAG}"
  [gitlab-webservice]="gitlab/gitlab/gitlab-rails:${IMAGE_TAG}"
  [gitlab-workhorse]="gitlab/gitlab/gitlab-base:${IMAGE_TAG}"
  [kubectl]="${UBI_IMAGE_MICRO_REPO}:${UBI_TAG}"
)
