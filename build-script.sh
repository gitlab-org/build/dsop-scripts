#!/usr/bin/env bash

set -euo pipefail

RELEASE_TAG="${1}"
REGISTRY="${REGISTRY:-local.localhost}"
IMAGE_TAG="${RELEASE_TAG%-*}"
IMAGE_TAG="${IMAGE_TAG#v*}"
ASSETS_ENDPOINT="${ASSETS_ENDPOINT:-https://gitlab-ubi.s3.us-east-2.amazonaws.com}"
ASSETS_ENDPOINT_PATH="${ASSETS_ENDPOINT_PATH:-ubi-build-dependencies-$RELEASE_TAG}"
ASSET_FILE="/tmp/deps-${RELEASE_TAG}.tar"
DOCKER_BUILD="${DOCKER_BUILD:-docker build}"
REPO_LIST="${2:-gitlab-base,kubectl,certificates,gitlab-kas,gitlab-ruby,gitlab-container-registry,gitlab-exporter,gitlab-mailroom,gitlab-shell,gitlab-pages,gitlab-rails,gitlab-workhorse,gitaly,gitlab-sidekiq,gitlab-toolbox,gitlab-webservice}"
IFS=',' read -ra repoList <<< "$REPO_LIST"

# shellcheck source=base-images.sh disable=SC1091
source "$(dirname "$0")"/base-images.sh

fetchAsset() {
  local ASSET_NAME="${1}"
  local ASSET_FILE="/tmp/deps-${RELEASE_TAG}-${ASSET_NAME}"

  if [ ! -f "${ASSET_FILE}" ]; then
    rm -f "${ASSET_FILE}"
    curl --fail --create-dirs "${ASSETS_ENDPOINT}/${ASSETS_ENDPOINT_PATH}/${ASSET_NAME}" -o "${ASSET_FILE}"
  fi

  cp "${ASSET_FILE}" "./${ASSET_NAME}"
}

fetchTini() {
  local ASSET_NAME="tini"
  local ASSET_FILE="/tmp/deps-${RELEASE_TAG}-${ASSET_NAME}"

  if [ ! -f "${ASSET_FILE}" ]; then
    rm -f "${ASSET_FILE}"
    curl \
      --location \
      --fail \
      --create-dirs \
      --output "${ASSET_FILE}" \
      "https://github.com/krallin/tini/releases/download/v0.19.0/tini-amd64"
  fi

  cp "${ASSET_FILE}" "./${ASSET_NAME}"
}

preappendRegistry() {
  local image="${1}"
  local fullImage="${REGISTRY}/${image}"

  echo "${fullImage#/}"
}

for repo in "${repoList[@]}"
do
  pushd "repo1/${repo}"

  mapfile -t resources < <(sed -rn 's/^ADD (.*.tar.gz).*$/\1/p' "Dockerfile")
  for resource in "${resources[@]}"
  do
     fetchAsset "${resource}"
  done
  if [[ "${repo}" == "gitlab-base" ]]; then
    fetchTini
  fi

  if [[ "${BASE_IMAGES[${repo}]}" =~ ^${UBI_IMAGE_BASE_REPO} ]]; then
    base_image="${BASE_IMAGES[${repo}]%:*}"
  else
    base_image="$(basename "${BASE_IMAGES[${repo}]%:*}")"
  fi

  declare -a  DOCKER_ARGS=(
    --progress=plain
    --provenance=false
    --build-arg BASE_REGISTRY="$REGISTRY"
    --build-arg BASE_IMAGE="${base_image}"
    --build-arg GITLAB_BASE_IMAGE="$(preappendRegistry "gitlab-base:${IMAGE_TAG}")"
    --build-arg GITLAB_RUBY_IMAGE="$(preappendRegistry "gitlab-ruby:${IMAGE_TAG}")"
    --build-arg RUBY_IMAGE="$(preappendRegistry "gitlab-ruby:${IMAGE_TAG}")"
    --build-arg GITLAB_RAILS_IMAGE="$(preappendRegistry "gitlab-rails:${IMAGE_TAG}")"
    --build-arg RAILS_IMAGE="$(preappendRegistry "gitlab-rails:${IMAGE_TAG}")"
    --build-arg UBI_IMAGE="${UBI_IMAGE_MINIMAL}"
    --build-arg UBI_IMAGE_MICRO="${UBI_IMAGE_MICRO}"
    --build-arg UBI_MICRO_IMAGE="${UBI_IMAGE_MICRO}"
  )

  DOCKER_BUILD="${DOCKER_BUILD}" DOCKER_OPTS="${DOCKER_ARGS[*]}" ./build-scripts/build.sh "${REGISTRY}" "${repo}" "${IMAGE_TAG}"

  popd
done

failedBuilds=$(find repo1 -maxdepth 2 -name failed.log)

if [ -n "$failedBuilds" ]; then
    echo -e "Some builds failed, check output the following files:\n$failedBuilds"
    exit 1;
fi
