#!/usr/bin/env bash

set -euxo pipefail

repoList=(
  "kubectl"
  "certificates"
  "gitlab-base"
  "gitlab-kas"
  "gitlab-ruby"
  "gitlab-container-registry"
  "gitlab-exporter"
  "gitlab-mailroom"
  "gitlab-shell"
  "gitlab-rails"
  "gitlab-workhorse"
  "gitaly"
  "gitlab-sidekiq"
  "gitlab-toolbox"
  "gitlab-webservice"
  "gitlab-pages"
)

for repo in "${repoList[@]}"
do
  if [ -d "repo1/${repo}" ]; then
    continue
  fi

  git clone "https://repo1.dso.mil/dsop/gitlab/distroless/${repo}.git" "repo1/${repo}"
  pushd "repo1/${repo}"
  git config commit.gpgsign true
  popd
done

if [ ! -d "gitlab-com/CNG" ]; then
  git clone "https://gitlab.com/gitlab-org/build/CNG.git" "gitlab-com/CNG"
fi
