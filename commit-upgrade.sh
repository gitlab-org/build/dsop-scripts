#!/usr/bin/env bash

set -euxo pipefail

# Make sure we don't commit any build assets
./clean-build.sh

BRANCH_NAME="${1:-gitlab-release}"
COMMIT_MESSAGE="${2:-Updated GitLab to the latest release}"

for dir in repo1/*/
do
  if [ ! -d "${dir}/.git" ]; then
    continue
  fi

  pushd "${dir}"
  if git show-ref refs/heads/"${BRANCH_NAME}"; then
      echo 'branch exists!'
  else
    git checkout -b "${BRANCH_NAME}"
    git add .
    git commit -m "${COMMIT_MESSAGE}"
  fi
  popd
done
