#!/bin/bash

repoList=(
	"kubectl"
	"certificates"
	"gitlab-base"
	"gitlab-kas"
	"gitlab-ruby"
	"gitlab-container-registry"
	"gitlab-exporter"
	"gitlab-mailroom"
	"gitlab-shell"
	"gitlab-rails"
	"gitlab-workhorse"
	"gitaly"
	"gitlab-sidekiq"
	"gitlab-toolbox"
	"gitlab-webservice"
	"gitlab-pages"
)

GITLAB_HOST=${GITLAB_HOST:-"https://repo1.dso.mil"}
GITLAB_TOKEN=${GITLAB_TOKEN:?"GITLAB_TOKEN is unset"}
export GITLAB_HOST GITLAB_TOKEN

GLAB=${GLAB:-"glab"}

GROUP_PATH="dsop/gitlab/distroless"
# DSO_ISSUE_TEMPLATE=${DSO_ISSUE_TEMPLATE:?"DSO_ISSUE_TEMPLATE is undefined. Download Issue template and point variable at it"}
DSO_ISSUE_LOG=${DSO_ISSUE_LOG:-${PWD}/dso-issues.log}
GITLAB_VERSION=${1:?"No version provided"}

get_template() {
	local _project_path
	_project_path=${1}
	local _project_id
	_project_id=$(printf %s "$_project_path" | jq -sRr @uri)
	${GLAB} api "projects/$_project_id/templates/issues/Application%20Update" | jq -r .content
}

for repo in "${repoList[@]}"; do
	#XXX We're relying on a very sketchy logic here: hoping that last MR filed is what we need
	MR_JSON=$(${GLAB} -R "${GROUP_PATH}/${repo}" -F json mr list | jq -r '[.[] | { "id": .id, "ref": .references.full, "title": .title}] | sort_by(.ref) | reverse | .[0]')
	ref=$(echo "$MR_JSON" | jq -r .ref)
	# Inject version into template, otherwise keep it intact
	description="$(get_template "${GROUP_PATH}/${repo}" | sed -e 's/^\([[:space:]]*Version:\) .*$/\1 '"${GITLAB_VERSION}"'/g')"
	# Append MR reference to issue description
	mr_string=$'\n\nMR:'" ${ref}"
	description="${description}${mr_string}"
	${GLAB} issue create -R "${GROUP_PATH}/${repo}" -t "${repo} ${GITLAB_VERSION}" -d $"${description}" -y | tee -a "${DSO_ISSUE_LOG}" 2>&1
done
