#!/usr/bin/env bash

### Run this to check if the system has all required dependencies.

set -euo pipefail

declare -A DEPENDENCIES
DEPENDENCIES=( ['bash']='4.0.0' \
               ['curl']='' \
               ['grep']='' \
               ['sed']='' \
               ['git']='' \
               ['yq']='4.0.0' \
               ['jq']='' \
               ['docker']=''
               )

countDots() {
  echo "${DEPENDENCIES["${DEPENDENCY}"]}" | grep -o '\.' | wc -l
}

dependencyTooOld() {
  printf "Dependency is too old, resolve by upgrading: %s from %s to %s\n" "${DEPENDENCY}" "${DEPENDENCY_VERSION}" "${DEPENDENCIES["${DEPENDENCY}"]}"
  FAIL_COUNTER=$((FAIL_COUNTER + 1))
}

dependencyMissing() {
  printf "Dependency is missing, resolve by installing: %s version %s or higher\n" "${DEPENDENCY}" "${DEPENDENCIES["${DEPENDENCY}"]}"
  FAIL_COUNTER=$((FAIL_COUNTER + 1))
}

installedMajorVersionCheck() {
  echo "${DEPENDENCY_VERSION}" | grep -o -P "^[[:digit:]]+(?=\.)"
}

installedMinorVersionCheck() {
  declare -i DOT_COUNT=${1}
  case ${DOT_COUNT} in
    1) echo "${DEPENDENCY_VERSION}" | grep -o -P "(?<=\.)[[:digit:]]+$";;
    2) echo "${DEPENDENCY_VERSION}" | grep -o -P "(?<=\.)[[:digit:]]+(?=\.)";;
  esac
}

installedPatchVersionCheck() {
  echo "${DEPENDENCY_VERSION}" | grep -o -P "(?<=\.)[[:digit:]]+$"
}

dependencyMajorVersionCheck() {
  echo "${DEPENDENCIES["${DEPENDENCY}"]}" | grep -o -P "^[[:digit:]]+(?=\.)"
}

dependencyMinorVersionCheck() {
  declare -i DOT_COUNT=${1}
  case ${DOT_COUNT} in
    1) echo "${DEPENDENCIES["${DEPENDENCY}"]}" | grep -o -P "(?<=\.)[[:digit:]]+$";;
    2) echo "${DEPENDENCIES["${DEPENDENCY}"]}" | grep -o -P "(?<=\.)[[:digit:]]+(?=\.)";;
  esac
}

dependencyPatchVersionCheck() {
  echo "${DEPENDENCIES["${DEPENDENCY}"]}" | grep -o -P "(?<=\.)[[:digit:]]+$"
}

dependencyCheck() {
  declare -i FAIL_COUNTER=0

  for DEPENDENCY in "${!DEPENDENCIES[@]}"; do
    DEPENDENCY_VERSION="$(${DEPENDENCY} --version | grep -m 1 -Eo "[[:digit:]]+\.[[:digit:]]+\.?[[:digit:]]*" | head -1)"
    if [[ ! -v DEPENDENCY_VERSION ]] || [[ -z "${DEPENDENCY_VERSION}" ]]; then
      dependencyMissing
    fi
    if [[ $(installedMajorVersionCheck) -lt $(dependencyMajorVersionCheck) ]]; then
      dependencyTooOld
    elif [[ $(installedMajorVersionCheck) -eq $(dependencyMajorVersionCheck) ]]; then
      if [[ $(installedMinorVersionCheck "$(countDots)") -lt $(dependencyMinorVersionCheck "$(countDots)") ]]; then
        dependencyTooOld
      elif [[ $(installedMinorVersionCheck "$(countDots)") -eq $(dependencyMinorVersionCheck "$(countDots)") ]]; then
        if [[ $(installedPatchVersionCheck) -lt $(dependencyPatchVersionCheck) ]]; then
          dependencyTooOld
        fi
      fi
    fi
  done

  if [[ "${FAIL_COUNTER}" -gt 0 ]]; then
    exit 1
  fi
}

dependencyCheck
