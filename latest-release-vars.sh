#!/usr/bin/env bash

set -euxo pipefail

RELEASE_ENDPOINT=${RELEASE_ENDPOINT-https://gitlab.com/api/v4/projects/4359271/releases}

# Only consider the 10 most recent releases (as returned by the api by date), sort them via version and grab the latest
LATEST_TAG=$(curl --fail --create-dirs "${RELEASE_ENDPOINT}" | jq -r 'map(.tag_name) | .[]' | head -n 10 | sort -V | tail -1)
LATEST_RELEASE=${LATEST_TAG%"-ubi"}
LATEST_VERSION=${LATEST_RELEASE#"v"}

cat << EOF > .latest_vars
LATEST_TAG="${LATEST_TAG}"
LATEST_RELEASE="${LATEST_RELEASE}"
LATEST_VERSION="${LATEST_VERSION}"
EOF
