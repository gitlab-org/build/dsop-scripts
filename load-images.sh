#!/usr/bin/env bash

set -euxo pipefail

RELEASE_VERSION="${1}"
IMAGE_LIST="${2}"
LOCAL_BASE_REGISTRY=${LOCAL_BASE_REGISTRY:-}
IMAGE_TAG="${IMAGE_TAG:-$(sed -e 's/^v\(.*\)/\1/; s/\(.*\)-.*/\1/' <<<"$RELEASE_VERSION")}"
IFS=',' read -ra imageList <<< "${IMAGE_LIST}"

mkdir -p images

for image in "${imageList[@]}"
do
  imageName="$(tail -n1 "images/${image}.txt")"
  docker pull "${CI_REGISTRY_IMAGE}/jobs/${imageName}"
  docker tag "${CI_REGISTRY_IMAGE}/jobs/${imageName}" "${LOCAL_BASE_REGISTRY:-}${LOCAL_BASE_REGISTRY:+/}${image}:${RELEASE_VERSION}"
done

# shellcheck source=base-images.sh disable=SC1091
source "$(dirname "$0")"/load-ubi-images.sh
