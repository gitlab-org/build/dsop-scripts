#!/usr/bin/env bash

set -euxo pipefail

IMAGE_TAG="${IMAGE_TAG:-not used}" # Keep base-images.sh happy.

# shellcheck source=base-images.sh disable=SC1091
source "$(dirname "$0")"/base-images.sh

docker pull "${REDHAT_UBI_IMAGE_MINIMAL}"
docker tag "${REDHAT_UBI_IMAGE_MINIMAL}" "${UBI_IMAGE_MINIMAL}"
docker pull "${REDHAT_UBI_IMAGE_MICRO}"
docker tag "${REDHAT_UBI_IMAGE_MICRO}" "${UBI_IMAGE_MICRO}"
