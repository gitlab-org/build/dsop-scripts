#!/usr/bin/env bash

set -euxo pipefail

BRANCH_NAME="${1:-gitlab-release}"
MR_TITLE="${2:-Update to gitlab version}"
MR_DESC="${3:-Adds the GitLab 17.7.0 Feature Release.<br><br>This is a regular monthly minor release of GitLab.<br><br>More information can be found at https://about.gitlab.com/releases/}"

for dir in repo1/*/
do
  if [ ! -d "${dir}/.git" ]; then
    continue
  fi

  pushd "${dir}"
  git config credential.helper 'cache --timeout 7200'
  git checkout "${BRANCH_NAME}"
  echo -e "\n"
  #git push -u origin  ${BRANCH_NAME}
  git push -u origin -o merge_request.create  -o merge_request.remove_source_branch=true -o merge_request.target=development -o merge_request.title="${MR_TITLE}" -o merge_request.description="${MR_DESC}" "${BRANCH_NAME}"
  popd
done
