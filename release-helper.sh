#!/usr/bin/env bash

set -euxo pipefail

RELEASE_TAG="${1}"
RELEASE_PATH="${2:-./repo1}"
SOURCE="${3:-./gitlab-com/CNG}"
IMAGE_TAG="${IMAGE_TAG:-$(sed -e 's/^v\(.*\)/\1/; s/\(.*\)-.*/\1/' <<<"$RELEASE_TAG")}"
BUILD_DEP_BUCKET_PREFIX="https://gitlab-ubi.s3.us-east-2.amazonaws.com/ubi-build-dependencies"
GITLAB_BASE_REPO="gitlab/gitlab/gitlab-base"
GITLAB_RUBY_REPO="gitlab/gitlab/gitlab-ruby"
GITLAB_RAILS_REPO="gitlab/gitlab/gitlab-rails"

pushd "${SOURCE}"
git checkout "${RELEASE_TAG}"
popd

DOCKERFILE_EXT='.ubi'

# shellcheck source=base-images.sh disable=SC1091
source "$(dirname "$0")"/base-images.sh

declare -A LABELED_VERSIONS=(
  [REGISTRY_VERSION]=''
  [GITLAB_EXPORTER_VERSION]=''
  [GITLAB_SHELL_VERSION]=''
  [GITALY_SERVER_VERSION]=''
  [MAILROOM_VERSION]=''
)

declare -a KEEP_PACKAGES=(
  "libtirpc"
  "libnsl2"
)

for _KEY in "${!LABELED_VERSIONS[@]}"; do
  LABELED_VERSIONS[$_KEY]=$(
    grep "${_KEY}" "${SOURCE}"/ci_files/variables.yml \
    | cut -d ':' -f 2 \
    | sed -e 's/[ \"]//g'
  )
done

fetchAssetsSHA() {
  declare ASSET_NAME="${1}"
  declare ASSET_SHA_FILE="/tmp/deps-${RELEASE_TAG}-${ASSET_NAME}.sha256"

  if [ ! -f "${ASSET_SHA_FILE}" ]; then
    rm -f "${ASSET_SHA_FILE}"
    curl --fail --create-dirs "${BUILD_DEP_BUCKET_PREFIX}-${RELEASE_TAG}/${ASSET_NAME}.sha256" -o "${ASSET_SHA_FILE}"
  fi
  cat "${ASSET_SHA_FILE}"
}

duplicateImageDir() {
  declare IMAGE_NAME="${1}"
  declare IMAGE_ROOT="${2}"
  if [ ! -f "${SOURCE}/${IMAGE_NAME}/Dockerfile${DOCKERFILE_EXT}" ]; then
    echo "Skipping ${IMAGE_NAME}"
    return 0
  fi
  mkdir -p "${IMAGE_ROOT}"
  cp -R "${SOURCE}/${IMAGE_NAME}"/* "${IMAGE_ROOT}"
  rm -f "${IMAGE_ROOT}"/{Dockerfile,"Dockerfile.build${DOCKERFILE_EXT}"}
  mv "${IMAGE_ROOT}/Dockerfile${DOCKERFILE_EXT}" "${IMAGE_ROOT}/Dockerfile"
}

prependBaseArgs() {
  declare DOCKERFILE="${1}"
  declare BASE_IMAGE="${2}"
  declare IMAGE_TAG="${3}"
  declare BASE_IMAGE_PATH="${4}"

  cat - "${DOCKERFILE}" > "${DOCKERFILE}.0" <<-EOF
ARG GITLAB_VERSION=${RELEASE_TAG}
ARG BASE_REGISTRY=registry1.dso.mil/ironbank
ARG BASE_IMAGE=${BASE_IMAGE}
ARG BASE_TAG=${IMAGE_TAG}
ARG UBI_TAG=${UBI_TAG}

EOF

  mv "${DOCKERFILE}.0" "${DOCKERFILE}"
}

substUBIImageArgs() {
  declare DOCKERFILE="${1}"
  if grep -sq 'ARG UBI_IMAGE=' "${DOCKERFILE}"; then
    sed -i "s!^ARG UBI_IMAGE=.*!ARG UBI_IMAGE=\${BASE_REGISTRY}/${UBI_IMAGE_MINIMAL_REPO}:\${UBI_TAG}!" "${DOCKERFILE}"
  fi
  if grep -sq 'ARG UBI_IMAGE_MICRO' "${DOCKERFILE}"; then
    sed -i "s!^ARG UBI_IMAGE_MICRO.*!ARG UBI_IMAGE_MICRO=\${BASE_REGISTRY}/${UBI_IMAGE_MICRO_REPO}:\${UBI_TAG}!" "${DOCKERFILE}"
  fi
  if grep -sq 'ARG UBI_MICRO_IMAGE' "${DOCKERFILE}"; then
    sed -i "s!^ARG UBI_MICRO_IMAGE.*!ARG UBI_MICRO_IMAGE=\${BASE_REGISTRY}/${UBI_IMAGE_MICRO_REPO}:\${UBI_TAG}!" "${DOCKERFILE}"
  fi
  if grep -sqF 'registry.access.redhat.com' "${DOCKERFILE}"; then
    # shellcheck disable=SC2016
    sed -i "s!^\(.*\)registry\.access\.redhat\.com/ubi9/ubi-\(.*\):[[:digit:]]\+\.[[:digit:]]\+\(.*\)\$!\1\${BASE_REGISTRY}/${UBI_IMAGE_BASE_REPO}/ubi9-\2:\${UBI_TAG}\3!" "${DOCKERFILE}"
  fi
}

replaceBaseImageArg() {
  declare DOCKERFILE="${1}"
  if grep -sq 'ARG GITLAB_BASE_IMAGE.*' "${DOCKERFILE}"; then
    sed -i "s!^ARG GITLAB_BASE_IMAGE.*!ARG GITLAB_BASE_IMAGE=\${BASE_REGISTRY}/${GITLAB_BASE_REPO}:\${BASE_TAG}!g" "${DOCKERFILE}"
  fi
}

replaceRubyImageArg() {
  declare DOCKERFILE="${1}"
  if grep -sq 'ARG RUBY_IMAGE.*' "${DOCKERFILE}"; then
    sed -i "s!^ARG RUBY_IMAGE.*!ARG RUBY_IMAGE=\${BASE_REGISTRY}/${GITLAB_RUBY_REPO}:\${BASE_TAG}!g" "${DOCKERFILE}"
  fi
}

replaceRailsImageArg() {
  declare DOCKERFILE="${1}"
  declare IMAGE_TAG="${2}"
  if grep -sq 'ARG RAILS_IMAGE.*' "${DOCKERFILE}"; then
    sed -i "s!^ARG RAILS_IMAGE.*!ARG RAILS_IMAGE=\${BASE_REGISTRY}/${GITLAB_RAILS_REPO}:\${BASE_TAG}!g" "${DOCKERFILE}"
  fi
}

replaceFinalStageFrom() {
  declare DOCKERFILE="${1}"

  # 1. Ensure that there is a final newline on the Dockerfile.
  # 2. Reveres the file.
  # 3. Use sed the replace the last FROM image variable (first after reversing)
  #    with ${BASE_REGISTRY}/${BASE_IMAGE}:\${BASE_TAG}. and
  #    leave the rest of the file alone.
  # 4 Reverse the output and send to a
  #   temporary file 5.Replace Dockerfile with temporary file.
  # shellcheck disable=SC1003
  sed '$a\' "${DOCKERFILE}" \
  | tac \
  | sed  '/FROM[[:space:]]\+\${.*}[[:blank:]]*/ {s!!FROM ${BASE_REGISTRY}/${BASE_IMAGE}:\${BASE_TAG} !;:loop; n; b loop}' \
  | tac > "${DOCKERFILE}.tmp"
  mv "${DOCKERFILE}.tmp" "${DOCKERFILE}"
}

replaceDNFArgs() {
  if grep -sq 'ARG DNF_OPTS[  ]*$' "${DOCKERFILE}"; then
    sed -i 's!^ARG DNF_OPTS[  ]*$!ARG DNF_OPTS=--disableplugin=subscription-manager!' "${DOCKERFILE}"
  fi
  if grep -sq 'ARG DNF_OPTS_ROOT[  ]*$' "${DOCKERFILE}"; then
    sed -i 's!^ARG DNF_OPTS_ROOT[  ]*$!ARG DNF_OPTS_ROOT="--installroot=/install-root --setopt=reposdir=/etc/yum.repos.d/ --setopt=cachedir=/install-cache/ --setopt=varsdir= --config= --noplugins"!' "${DOCKERFILE}"
  fi
  # DNF_INSTALL_ROOT= defined too late so remove definition and expand inline.
  if grep -sq 'ARG DNF_INSTALL_ROOT=.*$' "${DOCKERFILE}"; then
    sed -i '/^ARG DNF_INSTALL_ROOT=.*$/d' "${DOCKERFILE}"
  fi
  if grep -sq "\${DNF_INSTALL_ROOT}" "${DOCKERFILE}"; then
    sed -i "s!\${DNF_INSTALL_ROOT}!/install-root!g" "${DOCKERFILE}"
  fi
}

preserveKeepPackages() {
  declare DOCKERFILE="${1}"
  if grep -sq 'microdnf remove' "${DOCKERFILE}"; then
    for _PACKAGE in "${KEEP_PACKAGES[@]}"; do
      sed -i "s/${_PACKAGE}//g" "${DOCKERFILE}"
    done
  fi
}

replaceLabeledVersions() {
  declare DOCKERFILE="${1}"
  for _KEY in "${!LABELED_VERSIONS[@]}"; do
    sed -i "s/^ARG ${_KEY}.*/ARG ${_KEY}=${LABELED_VERSIONS[$_KEY]}/g" "${DOCKERFILE}"
  done

  # Remove any labels from the Dockerfile, as they should instead have been part of the manifest
  sed -i '/LABEL/,/^\s*$/{d}' "${DOCKERFILE}"
}

setFIPSMode() {
  declare DOCKERFILE="${1}"
  if grep -sq 'ARG FIPS_MODE=' "${DOCKERFILE}"; then
    sed -i 's/^ARG FIPS_MODE=.*/ARG FIPS_MODE=1/' "${DOCKERFILE}"
  fi
}

updateSources() {
  declare IMAGE_ROOT="${1}"

  cp -rv sources/* "$IMAGE_ROOT"/
  cp -rv sources/.[^.]* "$IMAGE_ROOT"/
}

updateBuildScripts() {
  declare IMAGE_TAG="${1}"
  declare IMAGE_ROOT="${2}"
  declare BUILD_SCRIPT="${IMAGE_ROOT}/build-scripts/build.sh"

  if [ -f "${BUILD_SCRIPT}" ]; then
    sed -i "s/^TAG=.*/TAG=\$\{3:-${IMAGE_TAG}\}/g" "${BUILD_SCRIPT}"
  fi
}

updateHardeningScripts() {
  declare IMAGE_ROOT="${1}"

  cp -r "${SOURCE}/hardening/." "${IMAGE_ROOT}/hardening/"
}

configureHardeningScripts() {
  declare DOCKERFILE="${1}"

  # We can't use mounts in DSOP builds, so we use copy/run/remove instead.
  if grep -sq 'RUN.*--mount.*target=/hardening' "${DOCKERFILE}"; then
    sed -i 's!^RUN.*--mount.*target=/hardening.*!COPY hardening /hardening!' "${DOCKERFILE}"
    sed -i 's!\(.*for f in /hardening.*\)!RUN \1; rm -rf /hardening!' "${DOCKERFILE}"
  fi
}

appendResourceToManifest() {
  declare RESOURCE_NAME="${1}"
  declare IMAGE_ROOT="${2}"
  declare ASSET_SHA
  ASSET_SHA=$(fetchAssetsSHA "${RESOURCE_NAME}")

  cat <<EOF >> "${IMAGE_ROOT}/hardening_manifest.yaml"
  - url: "${BUILD_DEP_BUCKET_PREFIX}-${RELEASE_TAG}/${RESOURCE_NAME}"
    filename: "${RESOURCE_NAME}"
    validation:
      type: "sha256"
      value: "${ASSET_SHA}"
EOF
}

# DSOP does not allow the use of curl in Docerfiles, so copy it in instead.
useCopyForTini() {
  declare IMAGE_ROOT="${1}"

  if [[ "$(basename "$IMAGE_ROOT")" != "gitlab-base" ]]; then
    return
  fi

  # Use COPY instead of curl in Dockerfile
  sed -i -e '/COPY scripts\/ \/install-root\/scripts/a COPY --chmod=755 ./tini /install-root/usr/bin/tini' "${DOCKERFILE}"
  sed -i -e '
    s/microdnf clean all \\/microdnf clean all/;
    /&& curl.*tini.*/d
    /&& chmod.*tini/d
  ' "${DOCKERFILE}"

  # Add tini to .gitignore.
  if ! grep tini "${IMAGE_ROOT}/.gitignore"; then
    echo "tini" >> "${IMAGE_ROOT}/.gitignore"
  fi

# Remove existing tini entry in the hardening manifest if it exists, and then
# adds it back it. Note that values are currently not quoted.
  yq '
  del(.resources[] | select(.filename=="tini"))
    | .resources += {
    "url":"https://github.com/krallin/tini/releases/download/v0.19.0/tini-amd64",
    "filename":"tini",
    "validation":{
      "type":"sha256",
      "value":"93dcc18adc78c65a028a84799ecf8ad40c936fdfc5f2a57b1acda5a8117fa82c"
    }
  }
  ' "${IMAGE_ROOT}/hardening_manifest.yaml" -i
}

# Requires https://github.com/mikefarah/yq (jq for yaml)
updateManifest()
{
  declare IMAGE_ROOT="${1}"
  declare BASE_IMAGE="${2}"
  declare BASE_IMAGE_TAG="${3}"
  declare UBI_TAG="${4}"

  BASE_IMAGE=${BASE_IMAGE} IMAGE_TAG=${IMAGE_TAG} BASE_IMAGE_TAG=${BASE_IMAGE_TAG} UBI_TAG=${UBI_TAG} yq eval '
    del(.resources) |
    .labels.["org.opencontainers.image.version"] = strenv(IMAGE_TAG) |
    .args.BASE_IMAGE = strenv(BASE_IMAGE) |
    .args.UBI_TAG = strenv(UBI_TAG) |
    .args.BASE_TAG = strenv(BASE_IMAGE_TAG) |
    .maintainers[0].email = "sterhar@gitlab.com" |
    .maintainers[0].name = "Steven Terhar" |
    .maintainers[0].username = "sterhar" |
    .maintainers[0].cht_member = false |
    (.tags.[] | select(. == "*.*.*")) = strenv(IMAGE_TAG)
    ' "${IMAGE_ROOT}/hardening_manifest.yaml" -i

  mapfile -t resources < <(sed -rn 's/^ADD (.*.tar.gz).*$/\1/p' "${IMAGE_ROOT}/Dockerfile")
  if [ ${#resources[@]} -eq 0 ]
  then
    echo "resources: []" >> "${IMAGE_ROOT}/hardening_manifest.yaml"
  else
    echo "resources: " >> "${IMAGE_ROOT}/hardening_manifest.yaml"
    for resource in "${resources[@]}"
    do
      appendResourceToManifest "${resource}" "${IMAGE_ROOT}"
    done
  fi
}

cleanupDirectory() {
  rm -rf "${IMAGE_ROOT}"/{patches,vendor,renderDockerfile,Dockerfile.erb,centos-8-base.repo}

  # remove old prepare scripts
  rm -f "${IMAGE_ROOT}/build-scripts/prepare.sh"
}

releaseImage() {
  declare IMAGE_NAME="${1%*-ee}"; shift
  declare BASE_IMAGE_PATH="${1-}"
  declare IMAGE_TAG="${RELEASE_TAG%-*}"
  IMAGE_TAG="${IMAGE_TAG#v*}"
  declare IMAGE_ROOT="${RELEASE_PATH}/${IMAGE_NAME}"
  declare DOCKERFILE="${IMAGE_ROOT}/Dockerfile"
  # shellcheck disable=SC2153
  declare BASE_TAG="${BASE_IMAGES[${IMAGE_NAME}]#*:}"
  # shellcheck disable=SC2153
  declare BASE_IMAGE="${BASE_IMAGES[${IMAGE_NAME}]%:*}"

  duplicateImageDir "${IMAGE_NAME}" "${IMAGE_ROOT}"
  prependBaseArgs "${DOCKERFILE}" "${BASE_IMAGE}" "${BASE_TAG}" "${BASE_IMAGE_PATH}"
  substUBIImageArgs "${DOCKERFILE}"
  replaceBaseImageArg "${DOCKERFILE}" "${IMAGE_TAG}"
  replaceRubyImageArg "${DOCKERFILE}" "${IMAGE_TAG}" "${BASE_IMAGE_PATH}"
  replaceRailsImageArg "${DOCKERFILE}" "${IMAGE_TAG}" "${BASE_IMAGE_PATH}"
  replaceFinalStageFrom "${DOCKERFILE}"
  replaceDNFArgs "${DOCKERFILE}"
  # Need to use fips images for setting FIPS_MODE=1.
  #setFIPSMode "${DOCKERFILE}"
  preserveKeepPackages "${DOCKERFILE}"
  updateSources "${IMAGE_ROOT}"
  updateBuildScripts "${IMAGE_TAG}" "${IMAGE_ROOT}"
  updateHardeningScripts "${IMAGE_ROOT}"
  configureHardeningScripts "${DOCKERFILE}"
  updateManifest "${IMAGE_ROOT}" "${BASE_IMAGE}" "${BASE_TAG}" "${UBI_TAG}" "${BASE_IMAGE_PATH}"
  useCopyForTini "${IMAGE_ROOT}"
  replaceLabeledVersions "${DOCKERFILE}"
  cleanupDirectory
}

mkdir -p "${RELEASE_PATH}"

releaseImage gitlab-base
releaseImage kubectl
releaseImage gitlab-kas
releaseImage certificates
releaseImage gitlab-ruby
releaseImage gitlab-workhorse-ee
releaseImage gitlab-pages
releaseImage gitlab-container-registry
releaseImage gitlab-shell
releaseImage gitaly
releaseImage gitlab-exporter
releaseImage gitlab-mailroom
releaseImage gitlab-rails-ee
releaseImage gitlab-webservice-ee
releaseImage gitlab-toolbox-ee
releaseImage gitlab-sidekiq-ee
