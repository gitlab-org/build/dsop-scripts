#!/usr/bin/env bash

set -euxo pipefail

RELEASE_VERSION="${1}"
IMAGE_LIST="${2}"
LOCAL_BASE_REGISTRY=${LOCAL_BASE_REGISTRY:-}
IFS=',' read -ra imageList <<< "${IMAGE_LIST}"

mkdir -p images

for image in "${imageList[@]}"
do
  targetImage="${CI_REGISTRY_IMAGE}/jobs/${image}:${CI_JOB_ID}"
  docker tag "${LOCAL_BASE_REGISTRY:-}${LOCAL_BASE_REGISTRY:+/}${image}:${RELEASE_VERSION}" "${targetImage}"
  docker push "${targetImage}"
  echo "${image}:${CI_JOB_ID}" > "images/${image}.txt"
done
