#!/bin/bash

# NOTICE: This script requires `docker`.

set -euxo pipefail

REGISTRY=${1:-}
REPOSITORY=${2:-"$(basename "$(builtin cd "$(dirname "$0")/.."; pwd)")"}
TAG=${3:-17.7.0}

DOCKER_BUILD="${DOCKER_BUILD:-docker build}"
DOCKER_OPTS=${DOCKER_OPTS:-""}

imageName() {
  local name="${REGISTRY}/${REPOSITORY}:${TAG}"

  echo "${name#/}"
}

buildImage() {
  local image="${1}"
  local context="${image%*-ee}"
  {
    # shellcheck disable=SC2086
    ${DOCKER_BUILD} \
      -t "$(imageName)" . \
      ${DOCKER_OPTS:-} | tee "${context}.out"
  } || {
    echo "${context}" >> failed.log
  }
}

# Cleanup log outputs from previous build
rm -f -- *.out failed.log

buildImage "${REPOSITORY}"
